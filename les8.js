var students = [];
var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 
						 'Глеб Стукалов', 30, 'Антон Павлович', 30, 
						 'Виктория Заровская', 30, 'Алексей Левенец', 70, 
						 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];

//Функция для вывода строки типа: Студент Виктория Заровская набрал 30 баллов
var commonShow = function() {
	console.log('Студент %s набрал %s баллов', this.name, this.point);
}

//Функция добавляет новых студентов в группу
var addStudents = function(studentName, studentPoint) {
	students.push({name: studentName, point: studentPoint, show: commonShow});
}

//Функция увеличивает баллы студентам
var updatePoints = function(studentName, studentPoint) {
	this.forEach(function(item, i) {
		if (students[i].name == studentName) {
			students[i].point += studentPoint;
		}
	});
}


//Заполняем массив students данными из studentsAndPoints
for (var i = 0; i < studentsAndPoints.length; i++) {
	students.push({name: studentsAndPoints[i], point: studentsAndPoints[++i], show: commonShow});
}

//Добавляем в список студентов «Николай Фролов» и «Олег Боровой». У них пока по 0 быллов
addStudents('Николай Фролов', 0);
addStudents('Олег Боровой', 0);

//Увеличить баллы студентам «Ирина Овчинникова» и «Александр Малов» на 30, а Николаю Фролову на 10
updatePoints.call(students, 'Ирина Овчинникова', 30);
updatePoints.call(students, 'Александр Малов', 30);
updatePoints.call(students, 'Николай Фролов', 10);

//Выводим список студентов набравших 30 и более баллов
students.forEach(function(item, i) {
	if (students[i].point >= 30) {
		console.log('Студент %s набрал %s баллов', students[i].name, students[i].point);
	}
});
console.log();

//Добавляем всем студентам поле worksAmount равное кол-ву сделанных работ из учета одна работа равна 10 баллам
students.forEach(function(item, i) {
	students[i].worksAmount = students[i].point / 10;
});